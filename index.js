let currentPage = 1;
let prevPage = 0;
const pages = [{
  id: 1,
  title: 'Общая информация',
  data: `Какая-то личная информация о пользователе`
},
{
  id: 2,
  title: 'История',
  data: `Какая-то история действий пользователя`
},
{
  id: 3,
  title: 'Друзья',
  data: 'Какие-то люди'
}];
const getPageById = (id) => pages.filter(item => item.id === id)[0];
const nav = () => {
  const menu = document.getElementById('tab')
  const newContent = pages.map(item => {
    const li = document.createElement("p"),
    text = document.createTextNode(item.title);
    li.append(text);
    li.id = item.id
    li.addEventListener('click', () => {
      currentPage = item.id;
      {
        if (prevPage != 0)
          document.getElementById(prevPage).classList = ''
        document.getElementById(currentPage).classList = 'active'
      }
      document.getElementById('content').innerHTML = '';
      document.getElementById('content').append(document.createTextNode(getPageById(item.id).data));
      prevPage = currentPage;
    });
    return li;
  })
  newContent.map(item => menu.append(item))
};
document.addEventListener("DOMContentLoaded", nav);